from fastapi import FastAPI
from pydantic import BaseModel, Field
from typing import Any


app = FastAPI(title="Playing Around")


@app.get("/square/{number}")
def square(number: int | float):  # int type is validated by fastapi
    return number**2


db_users = [
    {"id": 1, "first_name": "John", "last_name": "Doe", "age": 20},
    {"id": 2, "first_name": "Jane", "last_name": "Doe", "age": 21},
]


@app.get("/users/{id}")
def get_user_by_id(id: int):
    return [user for user in db_users if user.get("id") == id]


class UserName(BaseModel):
    first_name: str
    last_name: str


class User(BaseModel):
    name: UserName
    age: int = Field(ge=14)


class UserRes(BaseModel):
    status: int
    db: Any


# User model validates incoming request
# response_model validates outcoming response
@app.post("/users/{id}", response_model=UserRes)
def post_user(usr: User):
    db_users.append(
        {
            "id": db_users[-1].get("id") + 1,
            "first_name": usr.name.first_name,
            "last_name": usr.name.last_name,
            "age": usr.age,
        }
    )

    return {"status": 200, "db": db_users}


# TODO: more pydentic examples
